import * as Logger from "bunyan";
import { UserCreationService } from "./core/services/userCreationService";
import { UserRegistrationService } from "./core/services/userRegistrationService";
import { UserVerificationService } from "./core/services/userVerificationService";
import { ForgotPasswordService } from "./core/services/forgotPasswordService";
import { ResetPasswordService } from "./core/services/resetPasswordService";
import { ChangePasswordService } from "./core/services/changePasswordService";
import { LoginService } from "./core/services/loginService";

import { VersioningHasher } from "./security/versioningHasher";
import { RandomStringGenerator } from "./security/randomGenerators";
import { Pbkdf2Hasher } from "./security/pbkdf2Hasher";
export { IHasher } from "./core/security";

import { IUserCreationService, IUserRegistrationService, IUserVerificationService, IForgotPasswordService, IResetPasswordService, IChangePasswordService, ILoginService } from "./core/services";
export { IUserCreationService, IUserRegistrationService, IUserVerificationService, IForgotPasswordService, IResetPasswordService, IChangePasswordService, ILoginService };

export * from "./core/core";

import { IUserRepository, ITokenRepository } from "./core/data";
export { IUserRepository, ITokenRepository }
export { IDataContext } from "./core/data";

export interface IPbkdf2Config {
  iterations: number;
  outputBytes: number;
  hashAlgorithm: string;
  saltBytes: number;
}

export function makePbkdf2Hasher({ iterations, outputBytes, hashAlgorithm, saltBytes}: IPbkdf2Config) {
  return new Pbkdf2Hasher(new RandomStringGenerator(saltBytes), { iterations, outputBytes, hashAlgorithm });
}

const versioningHasher = new VersioningHasher()

// DON'T CHANGE
versioningHasher.register(1, makePbkdf2Hasher({ iterations: 10000, outputBytes: 256, hashAlgorithm: "sha512", saltBytes: 128 }));

export { versioningHasher }

export function makeUserCreationService(userRepository: IUserRepository, tokenRepository: ITokenRepository, logger: Logger, verificationTokenLength = 128): IUserCreationService {
  return new UserCreationService(
    userRepository,
    tokenRepository,
    new RandomStringGenerator(verificationTokenLength),
    versioningHasher,
    logger
  );
}

export function makeUserRegistrationService(userRepository: IUserRepository, logger: Logger): IUserRegistrationService {
  return new UserRegistrationService(
    userRepository,
    versioningHasher,
    logger
  );
}

export function makeUserVerificationService(userRepository: IUserRepository, tokenRepository: ITokenRepository, logger: Logger): IUserVerificationService {
  return new UserVerificationService(
    userRepository,
    tokenRepository,
    versioningHasher,
    logger
  );
}

export function makeLoginService(userRepository: IUserRepository, logger: Logger): ILoginService {
  return new LoginService(
    userRepository,
    versioningHasher,
    logger
  );
}

export function makeForgotPasswordService(userRepository: IUserRepository, tokenRepository: ITokenRepository, logger: Logger, passwordResetTokenLength = 128): IForgotPasswordService {
  return new ForgotPasswordService(
    userRepository,
    tokenRepository,
    new RandomStringGenerator(passwordResetTokenLength),
    versioningHasher,
    logger
  );
}

export function makeResetPasswordService(userRepository: IUserRepository, tokenRepository: ITokenRepository, logger: Logger): IResetPasswordService {
  return new ResetPasswordService(
    userRepository,
    tokenRepository,
    versioningHasher,
    logger
  );
}

export function makeChangePasswordService(userRepository: IUserRepository, tokenRepository: ITokenRepository, logger: Logger): IChangePasswordService {
  return new ChangePasswordService(
    userRepository,
    tokenRepository,
    versioningHasher,
    logger
  );
}
