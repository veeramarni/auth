import * as crypto from "crypto";

import { IVerificationTokenGenerator, IPasswordResetTokenGenerator, ISaltGenerator } from "../core/security";

export class RandomStringGenerator implements IVerificationTokenGenerator, IPasswordResetTokenGenerator, ISaltGenerator {
  constructor(private bytes: number) { }

  generateToken(): Promise<string> {
    return this.generateRandomString();
  }

  generateSalt(): Promise<string> {
    return this.generateRandomString();
  }

  private async generateRandomString(): Promise<string> {
    const bytes = await this.generateRandomBytes(this.bytes);
    return bytes.toString("base64");
  }

  private generateRandomBytes(bytes: number): Promise<Buffer> {
    return new Promise<Buffer>((res, rej) => {
      crypto.randomBytes(bytes, (err, data) => err ? rej(err) : res(data));
    });
  }
}
