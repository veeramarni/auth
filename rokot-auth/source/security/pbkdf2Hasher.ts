import * as crypto from "crypto";

import { IHasher, ISaltGenerator, IPbkdf2Config } from "../core/security";

export class Pbkdf2Hasher implements IHasher {
  constructor(
    private saltGenerator: ISaltGenerator,
    private config: IPbkdf2Config
  ) {
    if (config.iterations < 10000) {
      throw new Error(`Too few iterations, minimum: ${10000}`);
    }
    if (config.outputBytes < 256) {
      throw new Error(`Too few output bytes, minimum: ${256}`);
    }
  }

  async hash(input: string): Promise<string> {
    const salt = await this.saltGenerator.generateSalt();
    const hash = await this.hashImpl(input, salt);
    return `${hash}$${salt}`;
  }

  async compareHash(input: string, actualHashAndSalt: string): Promise<boolean> {
    const split = actualHashAndSalt.split("$");
    if (split.length != 2) {
      return false;
    }

    const actualHash = split[0];
    const salt = split[1];

    const hash = await this.hashImpl(input, salt);
    return crypto.timingSafeEqual(new Buffer(actualHash), new Buffer(hash));
  }

  async hashImpl(input: string, salt: string) {
    const buffer = await new Promise<Buffer>((res, rej) => {
      crypto.pbkdf2(input, salt, this.config.iterations, this.config.outputBytes, this.config.hashAlgorithm, (err: Error, hash: Buffer) => err ? rej(err) : res(hash))
    });

    return buffer.toString("base64");
  }
}
