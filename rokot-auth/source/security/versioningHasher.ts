import { IVersionedHash } from "../core/core";
import { IHasher } from "../core/security";

export class VersioningHasher {
  private hashers: IHasher[] = [];

  register(version: number, hasher: IHasher): void {
    this.hashers[version] = hasher;
  }

  getHasher(version: number): IHasher {
    return this.hashers[version];
  }

  getLatestHasherVersion(): number {
    return this.hashers.length - 1;
  }

  async hash(input: string): Promise<IVersionedHash> {
    const version = this.getLatestHasherVersion();
    const hasher = this.getHasher(version);
    const hash = await hasher.hash(input);
    return { hash, version };
  }

  async compareHash(input: string, actualHash: IVersionedHash): Promise<boolean> {
    const hasher = this.getHasher(actualHash.version);
    return hasher.compareHash(input, actualHash.hash);
  }

  async compareAllHashes(input: string, actualHashes: IVersionedHash[]): Promise<boolean> {
    for (const actualHash of actualHashes) {
      if (await this.compareHash(input, actualHash)) {
        return true;
      }
    }
    return false;
  }
}
