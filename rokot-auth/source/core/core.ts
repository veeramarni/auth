export interface IUser {
  id: string;
  username: string;
}

export interface INewUser {
  username: string;
  [key: string]: any;
}

export interface IUserCreationRequest {
  username: string;
  [key: string]: any;
}

export interface IUserRegistrationRequest {
  username: string;
  password: string;
  [key: string]: any;
}

export interface IUserCreationResponse {
  user: IUser;
  verificationToken: string;
}

export interface IUserRegistrationResponse {
  user: IUser;
}

export interface IVersionedHash {
  hash: string;
  version: number;
}

export interface IVerifyUserRequest {
  username: string;
  password: string;
  verificationToken: string;
}

export interface IResetPasswordRequest {
  username: string;
  password: string;
  passwordResetToken: string;
}

export interface IChangePasswordRequest {
  username: string;
  oldPassword: string;
  newPassword: string;
}

export interface ILoginRequest {
  username: string;
  password: string;
}

export interface IForgotPasswordRequest {
  username: string;
}

export interface IForgotPasswordResponse {
  username: string;
  passwordResetToken: string;
}
