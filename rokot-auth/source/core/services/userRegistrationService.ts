import * as Logger from "bunyan";
import { IUserRegistrationRequest, IUserRegistrationResponse, INewUser } from "../core";
import { IUserRepository, ITokenRepository, IDataContext } from "../data";
import { IVerificationTokenGenerator } from "../security";
import { IUserRegistrationService } from "../services";
import { VersioningHasher } from "../../security/versioningHasher";

export class UserRegistrationService implements IUserRegistrationService {
  constructor(
    private userRepository: IUserRepository,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async registerUser({ username, password, ...attributes }: IUserRegistrationRequest, dataContext?: IDataContext): Promise<IUserRegistrationResponse> {
    const userExists = await this.userRepository.userExists(username, dataContext);
    if (userExists) {
      this.logger.trace(`User registration failed: username '${username}' already taken`);
      throw new Error("User exists");
    }

    await this.userRepository.createUser({ username, ...attributes }, dataContext);

    const passwordHash = await this.hasher.hash(password);
    await this.userRepository.setUserPassword(username, passwordHash, dataContext);

    const user = await this.userRepository.getUserByUsername(username, dataContext);
    this.logger.trace(`User registration successful for user '${username}'`);
    return { user: user! };
  }
}
