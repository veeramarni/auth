import * as Logger from "bunyan";
import { IUserCreationRequest, IUserCreationResponse } from "../core";
import { IUserRepository, ITokenRepository, IDataContext } from "../data";
import { IVerificationTokenGenerator } from "../security";
import { IUserCreationService } from "../services";
import { VersioningHasher } from "../../security/versioningHasher";

export class UserCreationService implements IUserCreationService {
  constructor(
    private userRepository: IUserRepository,
    private tokenRepository: ITokenRepository,
    private verificationTokenGenerator: IVerificationTokenGenerator,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async createUser(creationRequest: IUserCreationRequest, dataContext?: IDataContext): Promise<IUserCreationResponse> {
    const userExists = await this.userRepository.userExists(creationRequest.username, dataContext);
    if (userExists) {
      this.logger.trace(`User creation failed: username '${creationRequest.username}' already taken`);
      throw new Error("User exists");
    }

    await this.userRepository.createUser(creationRequest, dataContext);

    const verificationToken = await this.createUserVerificationToken(creationRequest.username, dataContext)

    const user = await this.userRepository.getUserByUsername(creationRequest.username, dataContext);

    this.logger.trace(`User creation successful for user '${creationRequest.username}'`);
    return { user: user!, verificationToken };
  }

  async createUserVerificationToken(username: string, dataContext?: IDataContext): Promise<string> {
    const verificationToken = await this.verificationTokenGenerator.generateToken();
    const verificationTokenHash = await this.hasher.hash(verificationToken);

    await this.tokenRepository.createVerificationTokenHash(username, verificationTokenHash, dataContext);

    this.logger.trace(`User creation successful for user '${username}'`);
    return verificationToken;
  }
}
