import * as Logger from "bunyan";
import { IResetPasswordRequest } from "../core";
import { IResetPasswordService } from "../services";
import { IUserRepository, ITokenRepository, IDataContext } from "../data";
import { VersioningHasher } from "../../security/versioningHasher";

export class ResetPasswordService implements IResetPasswordService {
  constructor(
    private userRepository: IUserRepository,
    private tokenRepository: ITokenRepository,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async resetPassword({ username, password, passwordResetToken }: IResetPasswordRequest, dataContext?: IDataContext): Promise<void> {
    const userExists = await this.userRepository.userExists(username, dataContext);
    if (!userExists) {
      this.logger.trace(`Reset password failed: no such user '${username}'`);
      throw new Error("User doesn't exist");
    }

    const actualPasswordResetTokenHashes = await this.tokenRepository.getPasswordResetTokenHashes(username, dataContext);
    if (!actualPasswordResetTokenHashes.length) {
      this.logger.trace(`Reset password failed: user '${username}' hasn't requested a reset`);
      throw new Error("User doesn't have a token");
    }

    let valid = await this.hasher.compareAllHashes(passwordResetToken, actualPasswordResetTokenHashes);
    if (!valid) {
      this.logger.trace(`Reset password failed: token invalid for user '${username}'`);
      throw new Error("Invalid password reset token");
    }

    const passwordHash = await this.hasher.hash(password);

    await this.userRepository.setUserPassword(username, passwordHash, dataContext);
    await this.tokenRepository.destroyAllPasswordResetTokenHashes(username, dataContext);

    this.logger.trace(`Reset password successful for user '${username}'`);
  }
}
