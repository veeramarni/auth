import * as Logger from "bunyan";
import { IChangePasswordRequest } from "../core";
import { IChangePasswordService } from "../services";
import { IUserRepository, ITokenRepository, IDataContext } from "../data";
import { VersioningHasher } from "../../security/versioningHasher";

export class ChangePasswordService implements IChangePasswordService {
  constructor(
    private userRepository: IUserRepository,
    private tokenRepository: ITokenRepository,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async changePassword({ username, oldPassword, newPassword }: IChangePasswordRequest, dataContext?: IDataContext): Promise<void> {
    const userExists = await this.userRepository.userExists(username, dataContext);
    if (!userExists) {
      this.logger.trace(`Change password failed: no such user '${username}'`);
      throw new Error("User doesn't exist");
    }

    const actualPasswordHash = await this.userRepository.getPasswordHashByUsername(username, dataContext);
    const match = await this.hasher.compareHash(oldPassword, actualPasswordHash);
    if (!match) {
      this.logger.trace(`Change password failed: old password doesn't match for user '${username}'`);
      throw new Error("Old password doesn't match");
    }

    const newHash = await this.hasher.hash(newPassword);
    await this.userRepository.setUserPassword(username, newHash, dataContext);
    await this.tokenRepository.destroyAllPasswordResetTokenHashes(username, dataContext); // TODO: is this the right thing to do?

    this.logger.trace(`Change password successful for user '${username}'`);
  }
}
