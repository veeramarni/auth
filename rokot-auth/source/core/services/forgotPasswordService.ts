import * as Logger from "bunyan";
import { IForgotPasswordRequest, IForgotPasswordResponse } from "../core";
import { IForgotPasswordService } from "../services";
import { IUserRepository, ITokenRepository, IDataContext } from "../data";
import { IHasher, IPasswordResetTokenGenerator } from "../security";
import { VersioningHasher } from "../../security/versioningHasher";

export class ForgotPasswordService implements IForgotPasswordService {
  constructor(
    private userRepository: IUserRepository,
    private tokenRepository: ITokenRepository,
    private passwordResetTokenGenerator: IPasswordResetTokenGenerator,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async forgotPassword({ username }: IForgotPasswordRequest, dataContext?: IDataContext): Promise<IForgotPasswordResponse> {
    const userExists = await this.userRepository.userExists(username, dataContext);
    if (!userExists) {
      this.logger.trace(`Forgot password failed: no such user '${username}'`);
      throw new Error("user does not exist");
    }

    const passwordResetToken = await this.passwordResetTokenGenerator.generateToken();
    const hashedPasswordResetToken = await this.hasher.hash(passwordResetToken);

    await this.tokenRepository.createPasswordResetTokenHash(username, hashedPasswordResetToken, dataContext);

    this.logger.trace(`User '${username}' forgot password, reset token created`);
    return { username, passwordResetToken };
  }
}
