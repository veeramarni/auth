import * as Logger from "bunyan";
import { IVerifyUserRequest } from "../core";
import { IUserVerificationService } from "../services";
import { IUserRepository, ITokenRepository, IDataContext } from "../data";
import { VersioningHasher } from "../../security/versioningHasher";

export class UserVerificationService implements IUserVerificationService {
  constructor(
    private userRepository: IUserRepository,
    private tokenRepository: ITokenRepository,
    private hasher: VersioningHasher,
    private logger: Logger
  ) { }

  async verifyUser({ username, password, verificationToken }: IVerifyUserRequest, dataContext?: IDataContext): Promise<void> {
    const userExists = await this.userRepository.userExists(username, dataContext);
    if (!userExists) {
      this.logger.trace(`Verification failed: no such user '${username}'`);
      throw new Error("User doesn't exist");
    }

    const actualVerificationTokenHashes = await this.tokenRepository.getVerificationTokenHashes(username, dataContext);
    if (!actualVerificationTokenHashes.length) {
      this.logger.trace(`Verification failed: user '${username}' already verified`);
      throw new Error("User doesn't have a token");
    }

    let valid = await this.hasher.compareAllHashes(verificationToken, actualVerificationTokenHashes);
    if (!valid) {
      this.logger.trace(`Verification failed: token invalid for user '${username}'`);
      throw new Error("Invalid verification token");
    }

    const passwordHash = await this.hasher.hash(password);

    await this.userRepository.setUserPassword(username, passwordHash, dataContext);
    await this.tokenRepository.destroyAllVerificationTokenHashes(username, dataContext);

    this.logger.trace(`Verification successful for user '${username}'`);
  }
}
