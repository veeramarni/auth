import {
  IUser,
  IUserCreationRequest,
  IUserRegistrationRequest,
  IUserCreationResponse,
  IUserRegistrationResponse,
  IVerifyUserRequest,
  IForgotPasswordRequest,
  IForgotPasswordResponse,
  IResetPasswordRequest,
  IChangePasswordRequest,
  ILoginRequest
} from "./core";
import { IDataContext } from "./data";

export interface IUserCreationService {
  /**
   * Create a new user on their behalf. Pass through any additional fields that
   * need to be saved on the newUser object. Returns a verification token that
   * should be sent to the user over the same channel as password reset tokens
   * will be.
   */
  createUser(userCreation: IUserCreationRequest, dataContext?: IDataContext): Promise<IUserCreationResponse>;

  /**
   * Creates a user verification token
   * This can be used to create a token for an imported (migrated?) user that doesnt have a password
   */
  createUserVerificationToken(username: string, dataContext?: IDataContext): Promise<string>
}

export interface IUserRegistrationService {
  /**
   * Register a new user as the user themselves. Pass through any additional
   * fields that need to be saved on the newUser object. Returns a verification
   * token that should be sent to the user over the same channel as password
   * reset tokens will be.
   */
  registerUser(userRegistration: IUserRegistrationRequest, dataContext?: IDataContext): Promise<IUserRegistrationResponse>;
}

export interface ILoginService {
  /**
   * Login with a username and password. Retuns the user on successful login, or
   * null on failure.
   */
  login(loginRequest: ILoginRequest, dataContext?: IDataContext): Promise<IUser | null>;
}

export interface IUserVerificationService {
  /**
   * Verify a user's ability to receive messages over a side-channel, and set an
   * ititial password. Requires the vertification token which was sent to the
   * user as part of user creation.
   */
  verifyUser(verifyUserRequest: IVerifyUserRequest, dataContext?: IDataContext): Promise<void>;
}

export interface IForgotPasswordService {
  /**
   * Generate a password reset token for a user that has forgotten their
   * password. Returns a verification token that should be sent to the user over
   * the same channel as the vertification token was.
   */
  forgotPassword(forgotPasswordRequest: IForgotPasswordRequest, dataContext?: IDataContext): Promise<IForgotPasswordResponse>;
}

export interface IResetPasswordService {
  /**
   * Resets a user's password. Requires the password reset token which was sent
   * to the user as part of the forgot password flow.
   */
  resetPassword(resetPasswordRequest: IResetPasswordRequest, dataContext?: IDataContext): Promise<void>;
}

export interface IChangePasswordService {
  /**
   * Changes a user's password. Requires the existing password.
   * @param  {IChangePasswordRequest} changePasswordRequest [description]
   * @param  {IDataContext}           dataContext           [description]
   * @return {Promise<void>}                                [description]
   */
  changePassword(changePasswordRequest: IChangePasswordRequest, dataContext?: IDataContext): Promise<void>;
}
