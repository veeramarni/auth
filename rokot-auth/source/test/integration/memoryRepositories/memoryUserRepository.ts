import { IUser, INewUser, IVersionedHash } from "../../../core/core";
import { IUserRepository, IDataContext } from "../../../core/data";

export class MemoryUserRepository implements IUserRepository {
  private userStore = new Map<string, IUser>();
  private passwordStore = new Map<string, IVersionedHash>();

  async userExists(username: string, context: IDataContext): Promise<boolean> {
    return this.userStore.has(username);
  }

  async getUserByUsername(username: string, context: IDataContext): Promise<IUser | null> {
    return this.userStore.get(username) || null;
  }

  async getPasswordHashByUsername(username: string, context: IDataContext): Promise<IVersionedHash> {
    return this.passwordStore.get(username) as IVersionedHash;
  }

  async createUser(newUser: INewUser, context: IDataContext): Promise<void> {
    this.userStore.set(newUser.username, { username: newUser.username, id: Math.random().toString() });
  }

  async setUserPassword(username: string, passwordHash: IVersionedHash, context: IDataContext): Promise<void> {
    this.passwordStore.set(username, passwordHash);
  }
}
