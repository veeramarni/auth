import { IUser, INewUser, IVersionedHash } from "../../../core/core";
import { ITokenRepository, IDataContext } from "../../../core/data";

export class MemoryTokenRepository implements ITokenRepository {
  private verificationStore = new Map<string, IVersionedHash[]>();
  private passwordResetStore = new Map<string, IVersionedHash[]>();

  async getVerificationTokenHashes(username: string, context: IDataContext): Promise<IVersionedHash[]> {
    return this.verificationStore.get(username) || [];
  }

  async createVerificationTokenHash(username: string, verificationTokenHash: IVersionedHash, context: IDataContext): Promise<void> {
    const tokens = this.verificationStore.get(username);
    if (tokens) {
      tokens.push(verificationTokenHash);
    }
    else {
      this.verificationStore.set(username, [verificationTokenHash]);
    }
  }

  async destroyAllVerificationTokenHashes(username: string, context: IDataContext): Promise<void> {
    this.verificationStore.set(username, []);
  }

  async getPasswordResetTokenHashes(username: string, context: IDataContext): Promise<IVersionedHash[]> {
    return this.passwordResetStore.get(username) || [];
  }

  async createPasswordResetTokenHash(username: string, passwordResetTokenHash: IVersionedHash, context: IDataContext): Promise<void> {
    const tokens = this.passwordResetStore.get(username);
    if (tokens) {
      tokens.push(passwordResetTokenHash);
    }
    else {
      this.passwordResetStore.set(username, [passwordResetTokenHash]);
    }
  }

  async destroyAllPasswordResetTokenHashes(username: string, context: IDataContext): Promise<void> {
    this.passwordResetStore.set(username, []);
  }
}
