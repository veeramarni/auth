import { expect } from "rokot-test";

import { Pbkdf2Hasher } from "../../security/pbkdf2Hasher";
import { RandomStringGenerator } from "../../security/randomGenerators";

describe("Pbkdf2Hasher", () => {
  it("return true when comparing same input", async () => {
    const sut = new Pbkdf2Hasher(new RandomStringGenerator(128), { iterations: 10000, outputBytes: 256, hashAlgorithm: "sha512" });

    const hash = await sut.hash("The quick brown fox jumps over the lazy dog");
    const match = await sut.compareHash("The quick brown fox jumps over the lazy dog", hash);

    expect(match).to.be.true;
  });

  it("return false when comparing different input", async () => {
    const sut = new Pbkdf2Hasher(new RandomStringGenerator(128), { iterations: 10000, outputBytes: 256, hashAlgorithm: "sha512" });

    const hash = await sut.hash("The quick brown fox jumps over the lazy dog");
    const match = await sut.compareHash("The slow purple cat jumps over the busy mouse", hash);

    expect(match).to.be.false;
  });
});
