import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository, ITokenRepository } from "../../core/data";
import { IHasher } from "../../core/security";
import { UserVerificationService } from "../../core/services/userVerificationService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("UserVerificationService", () => {
  let sut: UserVerificationService;
  let setUserPasswordSpy: sinon.SinonSpy;
  let destroyAllVerificationTokenHashesSpy: sinon.SinonSpy;

  beforeEach(() => {
    const userExistsStub = sinon.stub();
    userExistsStub.withArgs("existing@user.com").returns(true);
    userExistsStub.withArgs("notoken@user.com").returns(true);
    userExistsStub.returns(false);

    setUserPasswordSpy = sinon.spy();

    const userRepository: IUserRepository = <any>{
      userExists: userExistsStub,
      setUserPassword: setUserPasswordSpy
    };

    const getVerificationTokenHashesStub = sinon.stub();
    getVerificationTokenHashesStub.withArgs("existing@user.com").returns([{ version: 1, hash: "hashed token" }]);
    getVerificationTokenHashesStub.returns(null);

    destroyAllVerificationTokenHashesSpy = sinon.spy();

    const tokenRepository: ITokenRepository = <any>{
      getVerificationTokenHashes: getVerificationTokenHashesStub,
      destroyAllVerificationTokenHashes: destroyAllVerificationTokenHashesSpy
    };

    const compareHashStub = sinon.stub();
    compareHashStub.withArgs("token", "hashed token").returns(true);
    compareHashStub.returns(false);

    const hasher: IHasher = {
      hash: sinon.stub().withArgs("password").returns("hashed password"),
      compareHash: compareHashStub
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    sut = new UserVerificationService(userRepository, tokenRepository, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#verifyUser", () => {
    it("calls setUserPassword on repository", async () => {
      await sut.verifyUser({ username: "existing@user.com", password: "password", verificationToken: "token" });
      expect(setUserPasswordSpy.calledWith("existing@user.com", { version: 1, hash: "hashed password" })).to.be.true;
    });

    it("calls destroyVerificationTokenHash on repository", async () => {
      await sut.verifyUser({ username: "existing@user.com", password: "password", verificationToken: "token" });
      expect(destroyAllVerificationTokenHashesSpy.calledWith("existing@user.com")).to.be.true;
    });

    it("throws when user doesn't exist", async () => {
      await expect(sut.verifyUser({ username: "new@user.com", password: "password", verificationToken: "token" })).to.be.rejected;
    });

    it("throws when user doesn't have a token", async () => {
      await expect(sut.verifyUser({ username: "notoken@user.com", password: "password", verificationToken: "token" })).to.be.rejected;
    });

    it("throws when token is invalid", async () => {
      await expect(sut.verifyUser({ username: "existing@user.com", password: "password", verificationToken: "fake token" })).to.be.rejected;
    });
  });
});
