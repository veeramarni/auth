import { expect, sinon } from "rokot-test";
import * as bunyan from "bunyan";

import { IUserRepository, ITokenRepository } from "../../core/data";
import { IHasher } from "../../core/security";
import { ResetPasswordService } from "../../core/services/resetPasswordService";
import { VersioningHasher } from "../../security/versioningHasher";

describe("ResetPasswordService", () => {
  let sut: ResetPasswordService;
  let setUserPasswordSpy: sinon.SinonSpy;
  let destroyAllPasswordResetTokenHashesSpy: sinon.SinonSpy;

  beforeEach(() => {
    const userExistsStub = sinon.stub();
    userExistsStub.withArgs("existing@user.com").returns(Promise.resolve(true));
    userExistsStub.withArgs("notoken@user.com").returns(Promise.resolve(true));
    userExistsStub.returns(Promise.resolve(false));

    setUserPasswordSpy = sinon.spy();

    const userRepository: IUserRepository = <any>{
      userExists: userExistsStub,
      setUserPassword: setUserPasswordSpy
    };

    const getPasswordResetTokenHashesStub = sinon.stub();
    getPasswordResetTokenHashesStub.withArgs("existing@user.com").returns(Promise.resolve([{ version: 1, hash: "hashed token" }]));
    getPasswordResetTokenHashesStub.returns(Promise.resolve(null));

    destroyAllPasswordResetTokenHashesSpy = sinon.spy();

    const tokenRepository: ITokenRepository = <any>{
      getPasswordResetTokenHashes: getPasswordResetTokenHashesStub,
      destroyAllPasswordResetTokenHashes: destroyAllPasswordResetTokenHashesSpy
    };

    const compareHashStub = sinon.stub();
    compareHashStub.withArgs("token", "hashed token").returns(Promise.resolve(true));
    compareHashStub.returns(Promise.resolve(false));

    const hasher: IHasher = {
      hash: sinon.stub().withArgs("password").returns("hashed password"),
      compareHash: compareHashStub
    };

    const vHasher = new VersioningHasher();
    vHasher.register(1, hasher);

    sut = new ResetPasswordService(userRepository, tokenRepository, vHasher, bunyan.createLogger({ name: "test" }));
  });

  describe("#resetPassword", () => {
    it("calls setUserPassword on repository", async () => {
      await sut.resetPassword({ username: "existing@user.com", password: "password", passwordResetToken: "token" });
      expect(setUserPasswordSpy.calledWith("existing@user.com", { version: 1, hash: "hashed password" })).to.be.true;
    });

    it("calls destroyPasswordResetToken on repository", async () => {
      await sut.resetPassword({ username: "existing@user.com", password: "password", passwordResetToken: "token" });
      expect(destroyAllPasswordResetTokenHashesSpy.calledWith("existing@user.com")).to.be.true;
    });

    it("throws when user doesn't exist", async () => {
      await expect(sut.resetPassword({ username: "new@user.com", password: "password", passwordResetToken: "token" })).to.be.rejected;
    });

    it("throws when user doesn't have a token", async () => {
      await expect(sut.resetPassword({ username: "notoken@user.com", password: "password", passwordResetToken: "token" })).to.be.rejected;
    });

    it("throws when token is invalid", async () => {
      await expect(sut.resetPassword({ username: "existing@user.com", password: "password", passwordResetToken: "fake token" })).to.be.rejected;
    });
  });
});
