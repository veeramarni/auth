# rokot-auth

Rokot - [Rocketmakers](http://www.rocketmakers.com/) TypeScript NodeJs Platform

## Introduction

A typescript library to help with username/password authentication.

rokot-auth deals with the core authentication flow, but leaves details like
user storage and session management up to you.

## Features

 * User creation/verification
 * Password hashing
 * Change password
 * Forgot/Reset password
 * Password hash versioning and auto-upgrade

## Installation

```
$ npm install --save rokot-auth
```

or if you're sufficiently bleeding-edge

```
$ yarn add rokot-auth
```

rokot-auth has a peer dependency on bunyan, so you will need to install bunyan
as well.

## Usage

The main functionality of the library is available through methods on service
classes. These classes must be constructed using provided factory methods,
passing in repositories which are used for data storage and retrieval, and
bunyan instances for logging. For example:

```typescript
const loginService = makeLoginService(new UserRespository(), logger);
```

The included typings for the repository interfaces should explain how to
implement your repositories.

### `LoginService.login`

This is used to check a user has provided valid credentials when they attempt
to login. Pass in the username and password provided by the user. Returns the
user if successful, or null if unsuccessful. Note: this method only checks the
credentials are valid, it is up to you what you do afterwards, e.g. create a
session or issue a token.

### `UserRegistrationService.registerUser`

This is used when a user is registering themselves with the app. Pass in the
desired username, password, and any additional attributes. The additional
attributes will be passed through to the repository. Returns the saved user if
successful, throws if unsuccessful.

### `UserCreationService.createUser`

This is used when a user is being registering by another user, e.g. an admin
creating an account for another user. Pass in the desired username, and any
additional attributes. The additional attributes will be passed through to the
repository. Returns the saved user and a verification token if successful,
throws if unsuccessful. The verification token must be sent to the user over
the same channel as will be used for password reset (probably email), and used
when calling the UserVerificationService.

### `UserVerificationService.registerUser`

This is used to verify that a user will be able to receive password reset
tokens if they forget their password. Pass in the username, desired password
and the verification token. Returns void if successful, throws if unsuccessful.

### `ChangePasswordService.changePassword`

This is used when a user changes their password. Pass in the username, current
password, and desired password. Returns void if successful, throws if unsuccessful.

### `ForgotPasswordService.forgotPassword`

This is used when a user hsa forgotten their password. Pass in the username.
Returns a password reset token if successful, throws if unsuccessful. The
verification token must be sent to the user over the same channel that is used
for user verification (probably email), and provided when calling the
ResetPasswordService.

### `ResetPasswordService.resetPassword`

This is used to complete the password reset process when a user forgets their
password. Pass in the username, desired password and the password reset token.
Returns void if successful, throws if unsuccessful.

## Password Hashing

By default, rokot-auth uses [PBKDF2](https://en.wikipedia.org/wiki/PBKDF2) for
password hashes. PBKDF2 is a key stretching algorithm, which means the password
is hashed many times in an attempt to slow down an attacker trying to brute-force
passwords.

The password hashing algorithm can be customized by registering a new
implementation of `IHasher` with the `VersioningHasher`. If you want to upgrade
the hashing algorithm for an existing application, register your hasher with a
higher version:

```typescript
const hasher = new MyNewHasher();
versioningHasher.register(2, hasher);
```

Any newly created passwords will use the hasher with the highest version
number, and any older password hashes will be rehashed when the user next logs
in.

The number of iterations used by PBKDF2 should be increased periodically. When
you want to do this, just register another hash version with a higher iteration
count.

## Using rokot-auth in an existing application

The easiest way is to implement a custom hasher with your existing password
hashing logic, and either register it as version 0 if you want to go forward
with rokot-auth's hashing (recommended), or as version 1 if you want to keep
your hashing as-is.

## Examples

In the examples directory, there is an example API that consumes rokot-auth, and
an example website that consumes the example API. The example API uses
[rokot-apicontroller](https://github.com/Rocketmakers/rokot-apicontroller), so
you will need a basic understanding of that library.
