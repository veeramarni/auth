CREATE TABLE profile (
  id serial PRIMARY KEY,
  display_name text NOT NULL
);

ALTER TABLE "user" ADD COLUMN profile_id int NOT NULL REFERENCES profile (id);
