CREATE TABLE "user" (
  id serial PRIMARY KEY,
  username text NOT NULL,
  password_hash text,
  verification_token_hash text
);
