declare const enum StateStatus {
  NotSet,
  Ok,
  Pending,
  Error
}

interface IStateFragment<T>{
  data: T;
  status: StateStatus;
}

interface IStoreAction<T> {
  type?: string;
  status?: StateStatus;
  payload?: T;
  promise?: Promise<T>;
  context?: any;
  thunk?: any;
}

interface ISiteState{
  users: IStateFragment<IUser[]>;
  profile: IStateFragment<IProfile>;
}
