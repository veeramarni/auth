import * as Redux from "redux";

export class StoreActionFactory {
  static api<T>(type: string, apiPromise: Promise<IFetchApiResponse<T>>, context?): IStoreAction<IFetchApiResponse<T>> {
    return { apiPromise, type, context } as any;
  }

  static promise<T>(type: string, promise: Promise<T>, context?): IStoreAction<T> {
    return { promise, type, context }
  }

  static action<T>(type: string, payload: T, context?): IStoreAction<T> {
    return { payload, type, context }
  }

  static thunk(thunk: (dispatch: Redux.Dispatch<ISiteState>, getState: () => ISiteState) => void): IStoreAction<any> {
    return { thunk }
  }
}
