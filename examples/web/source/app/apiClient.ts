import { ApiClientBase } from "./../core/api/apiClientBase";

class ApiClient extends ApiClientBase {
  public getUsers(count?: number): Promise<IFetchApiResponse<{ result: IUser[] }>> {
    return this.sendJson("GET", false, `?results=${count || 20}`)
  }
}

export default new ApiClient();
