import * as React from "react";
import { connect } from "react-redux";
import { ActionPromises } from './../redux/actionPromises';
import { Grid, Row, Col, Button, TextInput } from "armstrong-react";
import {Link} from "react-router";
interface ICurrentUserComponentProps {
  profile: IProfile;
  logout: () => void
}

class CurrentUserComponent extends React.Component<ICurrentUserComponentProps, void> {
  render() {
    const hasProfile = this.props.profile;
    const displayName =
      hasProfile ? ( <div><span>{this.props.profile.displayName}</span><span onClick={this.props.logout}> Logout </span></div>) :
      <Link className="fg-white" to="/login">Login</Link>

    return (
      <div>{displayName}</div>
    )
  }
}

const mapStateToProps = (state: ISiteState) => {
  return {
    profile: state.profile.data
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(ActionPromises.logout())
  }
}

export const CurrentUser = connect(
  mapStateToProps, mapDispatchToProps
)(CurrentUserComponent)
