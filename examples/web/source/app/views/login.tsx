import * as React from "react";
import { connect } from "react-redux";
import { ActionPromises } from './../redux/actionPromises';
import { Grid, Row, Col, Button, TextInput } from "armstrong-react";

interface ILoginComponentProps {
  login: (username: string, password: string) => void;
  loginJwt: (username: string, password: string) => void;
  profile: IProfile;
}

class LoginComponent extends React.Component<ILoginComponentProps, {username?: string, password?: string}> {
  constructor() {
    super();
    this.state = { username: "user", password: "password" };
  }

  render() {
    return (
      <div>
        <ul>{this.props.profile && this.props.profile && this.props.profile.displayName}</ul>
        <TextInput placeholder="username" value={this.state.username} onChange={e => this.setState({ username: e.target["value"] })} />
        <TextInput placeholder="password" value={this.state.password} onChange={e => this.setState({ password: e.target["value"] })} />
        <Button onClick={() => this.props.login(this.state.username, this.state.password)}>Login</Button>
        <Button onClick={() => this.props.loginJwt(this.state.username, this.state.password)}>Login Jwt</Button>
      </div>
    )
  }
}

const mapStateToProps = (state: ISiteState) => {
  return {
    profile: state.profile.data
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: (username: string, password: string) => dispatch(ActionPromises.login(username, password)),
    loginJwt: (username: string, password: string) => dispatch(ActionPromises.loginJwt(username, password))
  }
}

export const Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginComponent)
