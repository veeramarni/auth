import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Link, hashHistory, IndexRoute } from 'react-router';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { reducers } from './redux/reducers';
import { middlewares } from './../core/redux/middleware';
import { Grid, Row, Col } from "armstrong-react";

// VIEWS
import { Home } from "./views/home";
import { Login } from "./views/login";
import { NotFound } from "./views/system/notFound";
import {CurrentUser} from "./views/currentUser";
import {tokenStore} from "../api";
import {ActionPromises} from "./redux/actionPromises";
// STYLES
import "./theme/theme.scss";

// REDUX STORE
export default function configureStore(initialState) {
  const store = createStore(combineReducers(reducers), {}, compose(
    applyMiddleware(...middlewares),
    window["devToolsExtension"] ? window["devToolsExtension"]() : f => f
  ));

  if (tokenStore.get()) {
    store.dispatch<any>(ActionPromises.profileJwt())
  } else {
    store.dispatch<any>(ActionPromises.profile())
  }
  return store;
}

// APP WRAPPER
class App extends React.Component<any, {}> {
  public render() {
    const view = this.props.children;
    return (
      <Grid fillContainer={true}>
        <Row height="auto">
          <Col className="p-medium bg-brand-primary fg-white" horizontalAlignment="center" verticalAlignment="center">
            Welcome to rokot-auth-web-example
          </Col>
          <Col width={120} className="p-medium bg-brand-secondary fg-white" horizontalAlignment="right" verticalAlignment="center">
            <CurrentUser/>
          </Col>
        </Row>
        <Row>
          <Col className="p-medium">
            <main>
              { this.props.children }
            </main>
          </Col>
        </Row>
      </Grid>
    );
  }
}

ReactDOM.render(<Provider store={configureStore(window["__INITIAL_STATE__"]) }>
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="/login" component={Login} />
      <Route path="*" component={NotFound} />
    </Route>
  </Router>
</Provider>, document.getElementById('host'));
