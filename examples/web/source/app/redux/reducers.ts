import * as _ from "underscore";
import { Actions } from "./actions";
import { StateFragment, stateFragmentFactory } from "./../../core/redux/state";
import * as Redux from "redux";

function users(state = new StateFragment<IUser[]>([]), action: IStoreAction<any>): IStateFragment<IUser[]> {
  switch (action.type) {
    case Actions.GetUsers:
      return stateFragmentFactory(state, action, pl => pl.results);
  }
  return state;
}

function profile(state = new StateFragment<IProfile>(undefined), action: IStoreAction<any>): IStateFragment<IProfile> {
  switch (action.type) {
    case Actions.Profile:
      return stateFragmentFactory(state, action)
    case Actions.Logout:
      return new StateFragment<IProfile>(undefined)
  }
  return state;
}


const reducers : Redux.ReducersMapObject = { users, profile };
export { reducers };
