export class Actions {
  public static GetUsers = "GetUsers";
  public static Profile = "Profile";
  public static Logout = "Logout";
}
