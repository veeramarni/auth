export interface IProfile {
  id: number;
  displayName: string;
}

export interface ISocialUser {
  id: number;
  provider: string;
  providerId: string;
}

export interface INewUserRequest {
  username: string;
  displayName: string;
}
