declare module "connect-session-knex" {
  import express = require("express");
  import expressSession = require("express-session");
  import knex = require("knex");

  interface ExpressSessionStoreOptions {
    knex: knex,
    tableName?: string
  }

  class ExpressSessionStore extends expressSession.Store {
    constructor(options: ExpressSessionStoreOptions);
  }

  function connectSessionKnexFactory(session: typeof expressSession): typeof ExpressSessionStore;

  module connectSessionKnexFactory { }
  export = connectSessionKnexFactory;
}
