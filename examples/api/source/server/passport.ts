import * as express from 'express';
import * as expressSession from 'express-session';
import * as bodyParser  from 'body-parser';
import * as passport from 'passport';
import {Logger} from "rokot-log";
import {ILoginService, IUser} from "../../../../rokot-auth";
import {KnexConnection} from "../data/knexConnection";
import * as expressSessionKnexFactory from "connect-session-knex";
const KnexSessionStore = expressSessionKnexFactory(expressSession);

import {PostgresProfileRepository} from "../data/postgresProfileRepository";
import {PostgresSocialLoginRepository} from "../data/postgresSocialLoginRepository";
import { IProfile, ISocialUser } from "../core";

import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as TwitterStrategy, Profile as TwitterProfile } from "passport-twitter";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";

export class PassportAuth {
  constructor(private logger: Logger, private connection: KnexConnection, private profileRepository: PostgresProfileRepository, private loginService: ILoginService, private socialLoginRepository: PostgresSocialLoginRepository) { }

  private applyLocal() {
    this.logger.info("-- Local")
    passport.use(new LocalStrategy(async (username, password, done) => {
      let user: IUser | null;
      try {
        user = await this.loginService.login({ username, password });
      } catch (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false);
      }

      done(null, user);
    }));
  }

  private applyJwt(secretOrKey: string) {
    if (!secretOrKey) {
      this.logger.warn("-- Jwt auth not applied as no key provided!")
      return
    }
    this.logger.info("-- Jwt")
    passport.use(new JwtStrategy({
      secretOrKey,
      jwtFromRequest: ExtractJwt.fromAuthHeader()
    }, async (payload, done) => {
      let profile: IProfile | null;
      try {
        profile = await this.profileRepository.getProfileById(payload.profileId)
      } catch (err) {
        return done(err);
      }

      if (!profile) {
        return done(null, false);
      }
      done(null, profile);
    }))
  }

  private applyTwitter(consumerKey: string, consumerSecret: string) {
    if (!consumerKey || !consumerSecret) {
      this.logger.warn("-- Twitter auth not applied as no keys provided!")
      return
    }
    this.logger.info("-- Twitter")
    passport.use(new TwitterStrategy({
      consumerKey,
      consumerSecret,
      callbackURL: "http://localhost:3000/auth/twitter/callback"
    }, async (accessToken: string, refreshToken: string, profile: TwitterProfile, done: (error?: any, user?: any) => void) => {
      let user: ISocialUser;
      try {
        const profileId = await this.profileRepository.createProfile(profile.displayName);
        user = await this.socialLoginRepository.getOrCreateSocialUser(profileId, profile.provider, profile.id);
      } catch (err) {
        return done(err);
      }

      done(null, user);
    }));
  }

  setup(app: express.Express) {
    this.logger.info("Passport")
    this.applyLocal()
    this.applyTwitter(process.env.TWITTER_CONSUMER_KEY, process.env.TWITTER_CONSUMER_SECRET)

    this.applyJwt("supersecret")

    passport.serializeUser((user, done) => {
      done(null, user.profileId);
    });

    passport.deserializeUser(async (id, done) => {
      try {
        const profile = await this.profileRepository.getProfileById(id);
        done(null, profile);
      } catch (err) {
        done(err, null);
      }
    })

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(expressSession({ store: new KnexSessionStore({ knex: this.connection.knex, tableName: "api.sessions" }), secret: "topsecret", resave: false, saveUninitialized: false }));

    app.use(passport.initialize());
    app.use(passport.session());
  }
}
