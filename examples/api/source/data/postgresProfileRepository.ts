import { KnexConnection } from "./knexConnection";
import { IProfile } from "../core";

export class PostgresProfileRepository {
  constructor(private connection: KnexConnection){}
  async getProfileById(id: string): Promise<IProfile | null> {
    const rows: any[] = await this.connection.knex("profile").where({ id });
    if (!rows.length) {
      return null;
    }

    const row = rows[0];
    return {
      id: row["id"],
      displayName: row["display_name"]
    };
  }

  async createProfile(displayName: string, context?: any): Promise<number> {
    const result = await this.connection.knex("profile").insert({ display_name: displayName }).returning("id").transacting(context);
    return result[0];
  }
}
