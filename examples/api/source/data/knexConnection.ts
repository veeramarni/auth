import * as knexImport from "knex";
import {Logger} from "rokot-log";

export class KnexConnection {
  knex: knexImport;
  constructor(private logger: Logger, config :knexImport.Config){
    this.knex = knexImport(config);
  }

  async test() : Promise<boolean>{
    return await this.knex.raw('select 1 as result').then(() => {
      return true
    }).catch(e => {
      this.logger.error(e, "KNEX connection failed")
      return false
    });
  }

  async destroy() : Promise<void>{
    try{
      return await this.knex.destroy()
    }
    catch(e){
      this.logger.error(e, `Error while destroying knex connection`)
    }
  }
}
