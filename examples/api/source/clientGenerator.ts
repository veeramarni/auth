import {createDefaultApiClient} from "rokot-apicontroller";
import {expressClientWriter} from "rokot-apicontroller/lib/express/clientWriter";
import {IApiClient} from "rokot-apicontroller/lib/client/apiClientBuilder";
import "./controllers/authenticationController";
import "./controllers/jwtController";
import {ConsoleLogger} from "rokot-log";
import * as fs from "fs";
const logger = ConsoleLogger.create("create-api-client", { level: "trace" })
const ac = createDefaultApiClient(logger, [
  "./source/controllers/authenticationController.ts",
  "./source/controllers/jwtController.ts"
])

//return expressClientWriter(outputPath, apiClient, (t) => `IFetchApiResponse<${t}>`, `import {IFetchApiResponse} from "rokot-isomorphic-react"`).then(ok => {

expressClientWriter("../web/source/clientApi.d.ts", ac, {mode: "Definitions"}).then(ok => {
  logger.trace(ok ? "Api Client Defintions written ok" : "Api Client Defintions generation failed")
})

expressClientWriter("../web/source/clientApi.ts", ac, {mode: "Controllers", genericResponseWrapper: (t) => `IFetchApiResponse<${t}>`}, `/// <reference path="./clientApi.d.ts"/>`).then(ok => {
  logger.trace(ok ? "Api Client written ok" : "Api Client generation failed")
})
