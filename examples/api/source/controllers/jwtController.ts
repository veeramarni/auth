import { api } from "rokot-apicontroller";
import * as jwt from "jsonwebtoken";
import { KnexConnection } from "../data/knexConnection";
import {InjectTypes} from "../ioc/injectTypes";
import { IProfile } from "../core";

import {IRequest,IGetRequest} from "../server/expressRequest";

import {
  ILoginService,
  ILoginRequest
} from "../../../../rokot-auth";

@api.controller(InjectTypes.JwtController, "jwt")
export class JwtController {
  constructor(
    private connection: KnexConnection,
    private loginService: ILoginService
  ) { }

  @api.route("login")
  @api.verbs("post")
  async loginJwt(req: IRequest<ILoginRequest, { token: string }, void, void>) {
    await this.connection.knex.transaction(async (t) => {
      const result = await this.loginService.login(req.body);
      if (!result) {
        return req.send(400, "Nope");
      }

      const token = jwt.sign(result, "supersecret");
      req.sendOk({ token });
    });
  }

  @api.route("profiles/me")
  @api.verbs("get")
  @api.middleware("jwtAuthenticate")
  async getProfile(req: IGetRequest<IProfile, void, void>) {
    req.sendOk(req.native.request.user);
  }
}
