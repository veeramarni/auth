import "reflect-metadata";
import {run} from "./ioc/bootstrap";
import {ConsoleLogger} from "rokot-log";

const logger = ConsoleLogger.create("rokot-auth-example", {level: "trace"})
async function boot(){
  const ok = await run({
    client: "pg",
    connection: {
      host: '127.0.0.1', //"postgres"
      user: "postgres",
      password: "rokot",
      database: "rokot-auth-example"
    },
    searchPath: "api"
  },
  { port: 3000 },
  logger)

  if (!ok) {
    logger.error("Exiting process")
    process.exit(1)
  }
}

boot()
