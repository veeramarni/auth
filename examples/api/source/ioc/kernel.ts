import { Kernel, decorate, injectable, inject, interfaces } from "inversify";
export const kernel = new Kernel();

export function register(classType: any, key: string, ...ctorParams: string[]){
  decorate(injectable(), classType);
  ctorParams.forEach((c, i) => {
    decorate(inject(c), classType, i);
  })
  return kernel.bind(key).to(classType)
}
